package cz.muni.fi.pv168.hotelmanager;

import cz.muni.fi.pv168.hotelmanager.entity.Room;
import cz.muni.fi.pv168.hotelmanager.service.RoomManager;
import cz.muni.fi.pv168.hotelmanager.service.RoomManagerImpl;
import cz.muni.fi.pv168.hotelmanager.util.DBUtils;
import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.sql.DataSource;


public class RoomManagerImplTest {

    private RoomManagerImpl roomManager;
    private static final Comparator<Room> ROOM_ID_COMPARATOR = (g1, g2) -> g1.getId().compareTo(g2.getId());
    private DataSource dataSource;
    private Room room1;
    private Room room2;
    private Room room3;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws SQLException, ParseException {
        dataSource = prepareDataSource();
        DBUtils.executeSqlScript(dataSource, RoomManager.class.getResourceAsStream("/createTables.sql"));
        roomManager = new RoomManagerImpl(dataSource);

        room1 = newRoom(1L, 1, Room.Type.SINGLE, true, 200L);
        room2 = newRoom(2L, 2, Room.Type.DOUBLE, false, 400L);
        room3 = newRoom(3L, 5, Room.Type.SUITE, true, 900L);
    }

    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(dataSource, RoomManager.class.getResourceAsStream("/dropTables.sql"));
    }

    private static DataSource prepareDataSource() throws SQLException {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        dataSource.setUrl("jdbc:derby:memory:hoteldb-test;create=true");
        return dataSource;
    }

//    private static DataSource prepareDataSource() throws SQLException {
//        BasicDataSource dataSource = new BasicDataSource();
//        dataSource.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
//        dataSource.setUrl("jdbc:derby://localhost:1527/HotelDB");
//        dataSource.setUsername("martin");
//        dataSource.setPassword("admin");
//        return dataSource;
//    }

    @Test
    public void getRoomById() {
        roomManager.createRoom(room1);

        Long RoomId = room1.getId();
        Assert.assertNotNull(RoomId);
        Room result = roomManager.getRoomById(RoomId);

        assertRoomEquals(room1, result);
    }

    @Test
    public void getAllRooms() {
        roomManager.createRoom(room1);
        roomManager.createRoom(room2);
        roomManager.createRoom(room3);

        List<Room> expected = Arrays.asList(room1, room2, room3);
        List<Room> actual = roomManager.getAllRooms();

        Collections.sort(expected, ROOM_ID_COMPARATOR);
        Collections.sort(actual, ROOM_ID_COMPARATOR);

        for (int i = 0; i < expected.size(); i++) {
            assertRoomEquals(expected.get(i), actual.get(i));
        }
    }

    @Test
    public void updateRoom() throws ParseException {
        roomManager.createRoom(room2);
        roomManager.createRoom(room3);

        Long room2Id = room2.getId();
        room2 = roomManager.getRoomById(room2Id);
        room2.setBedCount(4);
        room2.setRoomType(Room.Type.STANDARD);
        room2.setClean(true);
        roomManager.updateRoom(room2);

        Room result = roomManager.getRoomById(room2Id);
        assertRoomEquals(room2, result);

        Long room3Id = room3.getId();
        room3 = roomManager.getRoomById(room3Id);
        room3.setBedCount(6);
        room3.setPricePerNight(1100L);
        roomManager.updateRoom(room3);

        result = roomManager.getRoomById(room3Id);
        assertRoomEquals(room3, result);
    }

    @Test
    public void deleteRoom() {
        roomManager.createRoom(room1);
        roomManager.createRoom(room3);

        Assert.assertNotNull(roomManager.getRoomById(room1.getId()));
        Assert.assertNotNull(roomManager.getRoomById(room3.getId()));

        roomManager.deleteRoom(room1.getId());

        List<Room> rooms = roomManager.getAllRooms();
        Assert.assertEquals(rooms.size(), 1);
        Assert.assertNotNull(roomManager.getRoomById(room3.getId()));
    }

    public static Room newRoom(Long id, int bedCount, Room.Type roomType, boolean clean, Long pricePerNight) {
        Room room = new Room();
        room.setId(id);
        room.setBedCount(bedCount);
        room.setRoomType(roomType);
        room.setClean(clean);
        room.setPricePerNight(pricePerNight);
        return room;
    }

    public static void assertRoomEquals(Room expected, Room result) {
        Assert.assertEquals(expected.getId(), result.getId());
        Assert.assertEquals(expected.getBedCount(), result.getBedCount());
        Assert.assertEquals(expected.getRoomType(), result.getRoomType());
        Assert.assertEquals(expected.isClean(), result.isClean());
        Assert.assertEquals(expected.getPricePerNight(), result.getPricePerNight());
    }
}
