package cz.muni.fi.pv168.hotelmanager;

import cz.muni.fi.pv168.hotelmanager.entity.Guest;
import cz.muni.fi.pv168.hotelmanager.service.GuestManager;
import cz.muni.fi.pv168.hotelmanager.service.GuestManagerImpl;
import cz.muni.fi.pv168.hotelmanager.util.DBUtils;
import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

public class GuestManagerImplTest {

    private GuestManagerImpl guestManager;
    private static final Comparator<Guest> GUEST_ID_COMPARATOR = (g1, g2) -> g1.getId().compareTo(g2.getId());
    private DataSource dataSource;
    private Guest guest1;
    private Guest guest2;
    private Guest guest3;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws SQLException, ParseException {
        dataSource = prepareDataSource();
        DBUtils.executeSqlScript(dataSource, GuestManager.class.getResourceAsStream("/createTables.sql"));
        guestManager = new GuestManagerImpl(dataSource);

        String date1 = "1988-02-15";
        String date2 = "1939-07-01";
        String date3 = "2003-09-10";

        guest1 = newGuest(1L, "Dave Lister", "Manchester, Chuckley road 23rd", dateFormat.parse(date1));
        guest2 = newGuest(2L, "Bruce Wayne", "Gotham, 3rd avenue, 675-a", dateFormat.parse(date2));
        guest3 = newGuest(3L, "Beatrix Kiddo", "Los Angeles, Hollywood way 9", dateFormat.parse(date3));
    }

    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(dataSource, GuestManager.class.getResourceAsStream("/dropTables.sql"));
    }

    private static DataSource prepareDataSource() throws SQLException {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        dataSource.setUrl("jdbc:derby:memory:hoteldb-test;create=true");
        return dataSource;
    }

//    private static DataSource prepareDataSource() throws SQLException {
//        BasicDataSource dataSource = new BasicDataSource();
//        dataSource.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
//        dataSource.setUrl("jdbc:derby://localhost:1527/HotelDB");
//        dataSource.setUsername("tomas");
//        dataSource.setPassword("admin");
//        return dataSource;
//    }

    @Test
    public void getGuestById() {
        guestManager.createGuest(guest1);

        Long guestId = guest1.getId();
        Assert.assertNotNull(guestId);
        Guest result = guestManager.getGuestById(guestId);

        assertGuestEquals(guest1, result);
    }

    @Test
    public void getGuestByName() {
        guestManager.createGuest(guest3);

        String guestName = guest3.getName();
        Assert.assertNotNull(guestName);
        Guest result = guestManager.getGuestByName(guestName);

        assertGuestEquals(guest3, result);
    }

    @Test
    public void getAllGuests() {
        guestManager.createGuest(guest1);
        guestManager.createGuest(guest2);
        guestManager.createGuest(guest3);

        List<Guest> expected = Arrays.asList(guest1, guest2, guest3);
        List<Guest> actual = guestManager.getAllGuests();

        Collections.sort(expected, GUEST_ID_COMPARATOR);
        Collections.sort(actual, GUEST_ID_COMPARATOR);

        for (int i = 0; i < expected.size(); i++) {
            assertGuestEquals(expected.get(i), actual.get(i));
        }
    }

    @Test
    public void updateGuest() throws ParseException {
        guestManager.createGuest(guest2);
        guestManager.createGuest(guest3);

        Long guest2Id = guest2.getId();
        guest2 = guestManager.getGuestById(guest2Id);
        guest2.setName("Batman");
        guest2.setAddress("Batcave");
        guestManager.updateGuest(guest2);

        Guest result = guestManager.getGuestById(guest2Id);
        assertGuestEquals(guest2, result);

        Long guest3Id = guest3.getId();
        guest3 = guestManager.getGuestById(guest3Id);
        guest3.setName("The Bride");
        guest3.setDateOfBirth(dateFormat.parse("2003-09-10"));
        guestManager.updateGuest(guest3);

        result = guestManager.getGuestById(guest3Id);
        assertGuestEquals(guest3, result);
    }

    @Test
    public void deleteGuest() {
        guestManager.createGuest(guest1);
        guestManager.createGuest(guest3);

        Assert.assertNotNull(guestManager.getGuestById(guest1.getId()));
        Assert.assertNotNull(guestManager.getGuestById(guest3.getId()));

        guestManager.deleteGuest(guest1.getId());

        List<Guest> guests = guestManager.getAllGuests();
        Assert.assertEquals(guests.size(), 1);
        Assert.assertNotNull(guestManager.getGuestById(guest3.getId()));
    }

    public static Guest newGuest(Long id, String name, String address, Date dateOfBirth) {
        Guest guest = new Guest();
        guest.setId(id);
        guest.setName(name);
        guest.setAddress(address);
        guest.setDateOfBirth(dateOfBirth);
        return guest;
    }

    public static void assertGuestEquals(Guest expected, Guest result) {
        Assert.assertEquals(expected.getId(), result.getId());
        Assert.assertEquals(expected.getName(), result.getName());
        Assert.assertEquals(expected.getAddress(), result.getAddress());
        Assert.assertEquals(expected.getDateOfBirth(), result.getDateOfBirth());
    }
}
