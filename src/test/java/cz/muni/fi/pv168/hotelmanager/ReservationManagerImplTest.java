package cz.muni.fi.pv168.hotelmanager;

import cz.muni.fi.pv168.hotelmanager.entity.Guest;
import cz.muni.fi.pv168.hotelmanager.entity.Reservation;
import cz.muni.fi.pv168.hotelmanager.entity.Room;
import cz.muni.fi.pv168.hotelmanager.service.GuestManager;
import cz.muni.fi.pv168.hotelmanager.service.GuestManagerImpl;
import cz.muni.fi.pv168.hotelmanager.service.ReservationManagerImpl;
import cz.muni.fi.pv168.hotelmanager.service.RoomManager;
import cz.muni.fi.pv168.hotelmanager.service.RoomManagerImpl;
import cz.muni.fi.pv168.hotelmanager.util.DBUtils;
import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

/**
 * Created by Martin on 29.03.2016.
 */
public class ReservationManagerImplTest {

    private ReservationManagerImpl reservationManager;
    private RoomManagerImpl roomManager;
    private GuestManagerImpl guestManager;
    private static final Comparator<Reservation> RESERVATION_ID_COMPARATOR = (g1, g2) -> g1.getId().compareTo(g2.getId());
    private DataSource dataSource;
    private Reservation reservation1;
    private Reservation reservation2;
    private Reservation reservation3;
    private Reservation reservation4;
    private Guest guest1;
    private Guest guest2;
    private Room room1;
    private Room room2;
    private Room room3;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws SQLException, ParseException {
        dataSource = prepareDataSource();
        DBUtils.executeSqlScript(dataSource, RoomManager.class.getResourceAsStream("/createTables.sql"));
        reservationManager = new ReservationManagerImpl(dataSource);
        roomManager = new RoomManagerImpl(dataSource);
        guestManager = new GuestManagerImpl(dataSource);

        String date1 = "1988-02-15";
        String date2 = "1939-07-01";
        String date3 = "2003-09-10";

        // change constructor later
        guest1 = new Guest(1L, "Dave Lister", "Manchester, Chuckley road 23rd", dateFormat.parse(date1));
        guest2 = new Guest(2L, "Bruce Wayne", "Gotham, 3rd avenue, 675-a", dateFormat.parse(date2));

        room1 = new Room(1L, 1, Room.Type.SINGLE, true, 200L);
        room2 = new Room(2L, 2, Room.Type.DOUBLE, false, 400L);
        room3 = new Room(3L, 5, Room.Type.SUITE, true, 900L);

        reservation1 = newReservation(1L, dateFormat.parse(date2), dateFormat.parse(date1), room2.getId(),
                guest1.getId(), true);
        reservation2 = newReservation(2L, dateFormat.parse(date1), dateFormat.parse(date3), room3.getId(),
                guest1.getId(), true);
        reservation3 = newReservation(3L, dateFormat.parse(date2), dateFormat.parse(date1), room1.getId(),
                guest2.getId(), true);
        reservation4 = newReservation(4L, dateFormat.parse(date1), dateFormat.parse(date3), room1.getId(),
                guest2.getId(), true);
    }

    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(dataSource, GuestManager.class.getResourceAsStream("/dropTables.sql"));
    }

    private static DataSource prepareDataSource() throws SQLException {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        dataSource.setUrl("jdbc:derby:memory:hoteldb-test;create=true");
        return dataSource;
    }

//    private static DataSource prepareDataSource() throws SQLException {
//        BasicDataSource dataSource = new BasicDataSource();
//        dataSource.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
//        dataSource.setUrl("jdbc:derby://localhost:1527/HotelDB");
//        dataSource.setUsername("martin");
//        dataSource.setPassword("admin");
//        return dataSource;
//    }

    @Test
    public void getReservationById() {
        reservationManager.createReservation(reservation1);

        Long reservationId = reservation1.getId();
        Assert.assertNotNull(reservationId);
        Reservation result = reservationManager.getReservationById(reservationId);

        assertReservationEquals(reservation1, result);
    }

    @Test
    public void getAllReservations() {
        reservationManager.createReservation(reservation1);
        reservationManager.createReservation(reservation2);
        reservationManager.createReservation(reservation3);

        List<Reservation> expected = Arrays.asList(reservation1, reservation2, reservation3);
        List<Reservation> actual = reservationManager.getAllReservations();

        Collections.sort(expected, RESERVATION_ID_COMPARATOR);
        Collections.sort(actual, RESERVATION_ID_COMPARATOR);

        for (int i = 0; i < expected.size(); i++) {
            assertReservationEquals(expected.get(i), actual.get(i));
        }
    }

    @Test
    public void getReservationsByRoom() {
        reservationManager.createReservation(reservation3);
        reservationManager.createReservation(reservation4);
        roomManager.createRoom(room1);
        guestManager.createGuest(guest2);

        Long roomId = reservation3.getRoomId();
        Assert.assertNotNull(roomId);

        List<Reservation> expected = Arrays.asList(reservation3, reservation4);
        List<Reservation> actual = reservationManager.getReservationsByRoom(roomId);

        Collections.sort(expected, RESERVATION_ID_COMPARATOR);
        Collections.sort(actual, RESERVATION_ID_COMPARATOR);

        for (int i = 0; i < expected.size(); i++) {
            assertReservationEquals(expected.get(i), actual.get(i));
        }
    }

    @Test
    public void getReservationsByGuest() {
        reservationManager.createReservation(reservation1);
        reservationManager.createReservation(reservation2);
        roomManager.createRoom(room3);
        roomManager.createRoom(room2);
        guestManager.createGuest(guest1);

        Long guestId = reservation3.getRoomId();
        Assert.assertNotNull(guestId);

        List<Reservation> expected = Arrays.asList(reservation1, reservation2);
        List<Reservation> actual = reservationManager.getReservationsByGuest(guestId);

        Collections.sort(expected, RESERVATION_ID_COMPARATOR);
        Collections.sort(actual, RESERVATION_ID_COMPARATOR);

        for (int i = 0; i < expected.size(); i++) {
            assertReservationEquals(expected.get(i), actual.get(i));
        }
    }

    @Test
    public void editReservation() throws ParseException {
        reservationManager.createReservation(reservation1);

        Long reservationId = reservation1.getId();
        Assert.assertNotNull(reservationId);
        reservation1 = reservationManager.getReservationById(reservationId);
        reservation1.setEndDate(dateFormat.parse("2013-09-10"));
        reservation1.setIsOver(true);
        reservationManager.editReservation(reservation1);

        Reservation result = reservationManager.getReservationById(reservationId);
        assertReservationEquals(reservation1, result);
    }

    @Test
    public void endReservation() {
        reservationManager.createReservation(reservation1);

        Long reservationId = reservation1.getId();
        Assert.assertNotNull(reservationId);

        reservationManager.endReservation(reservation1);

        Reservation result = reservationManager.getReservationById(reservationId);
        Assert.assertTrue(result.isOver());
    }

    public static Reservation newReservation(Long id, Date start, Date end, Long roomId, Long guestId, boolean isOver) {
        Reservation reservation = new Reservation();
        reservation.setId(id);
        reservation.setStartDate(start);
        reservation.setEndDate(end);
        reservation.setRoomId(roomId);
        reservation.setGuestId(guestId);
        reservation.setIsOver(isOver);
        return reservation;
    }

    public static void assertReservationEquals(Reservation expected, Reservation result) {
        Assert.assertEquals(expected.getId(), result.getId());
        Assert.assertEquals(expected.getStartDate(), result.getStartDate());
        Assert.assertEquals(expected.getEndDate(), result.getEndDate());
        Assert.assertEquals(expected.isOver(), result.isOver());
        Assert.assertEquals(expected.getRoomId(), result.getRoomId());
        Assert.assertEquals(expected.getGuestId(), result.getGuestId());
    }
}
