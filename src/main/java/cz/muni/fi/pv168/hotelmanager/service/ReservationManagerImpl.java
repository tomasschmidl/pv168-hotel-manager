package cz.muni.fi.pv168.hotelmanager.service;

import cz.muni.fi.pv168.hotelmanager.entity.Reservation;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.List;

import javax.sql.DataSource;

public class ReservationManagerImpl implements ReservationManager {

    private JdbcTemplate jdbc;

    public ReservationManagerImpl(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    public void createReservation(Reservation reservation) {
        SimpleJdbcInsert insertReservation = new SimpleJdbcInsert(jdbc)
                .withTableName("reservations").usingGeneratedKeyColumns("id");

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("start_date", reservation.getStartDate())
                .addValue("end_date", reservation.getEndDate())
                .addValue("room_id", reservation.getRoomId())
                .addValue("guest_id", reservation.getGuestId())
                .addValue("is_over", reservation.isOver());

        Number id = insertReservation.executeAndReturnKey(parameters);
        reservation.setId(id.longValue());
    }

    public void editReservation(Reservation reservation) {
        jdbc.update("UPDATE reservations SET start_date=?, end_date=?, room_id=?, guest_id=?, is_over=?",
                reservation.getStartDate(),
                reservation.getEndDate(),
                reservation.getRoomId(),
                reservation.getGuestId(),
                reservation.isOver());
    }

    public List<Reservation> getReservationsByRoom(Long roomId) {
        return jdbc.query("SELECT * FROM reservations WHERE room_id=?", reservationMapper, roomId);
    }

    public List<Reservation> getReservationsByGuest(Long guestId) {
        return jdbc.query("SELECT * FROM reservations WHERE guest_id=?", reservationMapper, guestId);
    }

//    public List<Reservation> getReservationsByDate(Date from, Date to) {
//        return jdbc.queryForObject("SELECT * FROM reservations WHERE start=?, end=?", reservationMapper, from, to);
//    }

    public Reservation getReservationById(Long id) {
        return jdbc.queryForObject("SELECT * FROM reservations WHERE id=?", reservationMapper, id);
    }

    public List<Reservation> getAllReservations() {
        return jdbc.query("SELECT * FROM reservations", reservationMapper);
    }

    public void endReservation(Reservation reservation) {
        jdbc.update("UPDATE reservations SET is_over=?", reservation.isOver());
    }

    private RowMapper<Reservation> reservationMapper = (rs, rowNum) ->
            new Reservation(rs.getLong("id"),
                    rs.getDate("start_date"),
                    rs.getDate("end_date"),
                    rs.getLong("room_id"),
                    rs.getLong("guest_id"),
                    rs.getBoolean("is_over"));
}
