package cz.muni.fi.pv168.hotelmanager.service;

import cz.muni.fi.pv168.hotelmanager.entity.Guest;

import java.util.List;

public interface GuestManager {

    void createGuest(Guest guest);

    Guest getGuestById(Long id);

    Guest getGuestByName(String name);

    List<Guest> getAllGuests();

    void updateGuest(Guest guest);

    void deleteGuest(Long id);
}
