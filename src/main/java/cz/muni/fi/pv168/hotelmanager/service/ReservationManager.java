package cz.muni.fi.pv168.hotelmanager.service;

import cz.muni.fi.pv168.hotelmanager.entity.Reservation;

import java.util.List;

public interface ReservationManager {
    void createReservation(Reservation reservation);

    void editReservation(Reservation reservation);

    List<Reservation> getReservationsByRoom(Long roomId);

    List<Reservation> getReservationsByGuest(Long guestId);

    //List<Reservation> getReservationsByDate(Date from, Date to);

    Reservation getReservationById(Long id);

    List<Reservation> getAllReservations();

    void endReservation(Reservation reservation);
}
