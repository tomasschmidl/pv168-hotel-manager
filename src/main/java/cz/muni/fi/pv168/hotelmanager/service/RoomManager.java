package cz.muni.fi.pv168.hotelmanager.service;

import cz.muni.fi.pv168.hotelmanager.entity.Room;

import java.util.List;

public interface RoomManager {
    void createRoom(Room room);

    Room getRoomById(Long id);

    List<Room> getAllRooms();

    void updateRoom(Room room);

    void deleteRoom(Long id);
}
