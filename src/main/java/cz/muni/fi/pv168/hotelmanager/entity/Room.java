package cz.muni.fi.pv168.hotelmanager.entity;

import java.util.HashMap;
import java.util.Map;

public class Room {
    private Long id;
    private int bedCount;
    private Type roomType;
    private boolean clean;
    private Long pricePerNight;

    public Room() {}

    public Room(Long id, int bedCount, Type roomType, boolean clean, Long pricePerNight) {
        this.id = id;
        this.bedCount = bedCount;
        this.roomType = roomType;
        this.clean = clean;
        this.pricePerNight = pricePerNight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getBedCount() {
        return bedCount;
    }

    public void setBedCount(int bedCount) {
        this.bedCount = bedCount;
    }

    public Type getRoomType() {
        return roomType;
    }

    public void setRoomType(Type roomType) {
        this.roomType = roomType;
    }

    public boolean isClean() {
        return clean;
    }

    public void setClean(boolean clean) {
        this.clean = clean;
    }

    public Long getPricePerNight() {
        return pricePerNight;
    }

    public void setPricePerNight(Long pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", bedCount=" + bedCount +
                ", roomType=" + roomType +
                ", clean=" + clean +
                ", pricePerNight=" + pricePerNight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;

        Room room = (Room) o;

        if (bedCount != room.bedCount) return false;
        if (clean != room.clean) return false;
        if (id != null ? !id.equals(room.id) : room.id != null) return false;
        if (roomType != room.roomType) return false;
        return !(pricePerNight != null ? !pricePerNight.equals(room.pricePerNight) : room.pricePerNight != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + bedCount;
        result = 31 * result + (roomType != null ? roomType.hashCode() : 0);
        result = 31 * result + (clean ? 1 : 0);
        result = 31 * result + (pricePerNight != null ? pricePerNight.hashCode() : 0);
        return result;
    }

    public enum Type {

        SINGLE("SINGLE"),
        DOUBLE("DOUBLE"),
        TRIPLE("TRIPLE"),
        STANDARD("STANDARD"),
        SUITE("SUITE"),
        ROYAL_SUITE("ROYAL_SUITE");

        private final String value;
        private static Map<String, Room.Type> constants = new HashMap<String, Room.Type>();

        static {
            for (Room.Type c: values()) {
                constants.put(c.value, c);
            }
        }

        Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public static Room.Type fromValue(String value) {
            Room.Type constant = constants.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
