package cz.muni.fi.pv168.hotelmanager.service;

import cz.muni.fi.pv168.hotelmanager.entity.Guest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.sql.DataSource;

public class GuestManagerImpl implements GuestManager {

    final static Logger log = LoggerFactory.getLogger(GuestManagerImpl.class);

    private JdbcTemplate jdbc;

    public GuestManagerImpl(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    public void createGuest(Guest guest) {
        log.debug("createGuest({})", guest);
        SimpleJdbcInsert insertGuest = new SimpleJdbcInsert(jdbc)
                .withTableName("guests").usingGeneratedKeyColumns("id");

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name", guest.getName())
                .addValue("address", guest.getAddress())
                .addValue("date_of_birth", guest.getDateOfBirth());

        Number id = insertGuest.executeAndReturnKey(parameters);
        guest.setId(id.longValue());
    }

    public Guest getGuestById(Long id) {
        log.debug("getGuestById({})", id);
        return jdbc.queryForObject("SELECT * FROM guests WHERE id=?", guestMapper, id);
    }

    public Guest getGuestByName(String name) {
        log.debug("getGuestByName({})", name);
        return jdbc.queryForObject("SELECT * FROM guests WHERE name=?", guestMapper, name);
    }

    @Transactional
    public List<Guest> getAllGuests() {
        log.debug("getAllGuests");
        return jdbc.query("SELECT * FROM guests", guestMapper);
    }

    public void updateGuest(Guest guest) {
        log.debug("updateGuest({})", guest);
        jdbc.update("UPDATE guests SET name=?,address=?,date_of_birth=?",
                guest.getName(), guest.getAddress(), guest.getDateOfBirth());
    }

    public void deleteGuest(Long id) {
        log.debug("deleteGuest({})", id);
        jdbc.update("DELETE FROM guests WHERE id=?", id);
    }

    private RowMapper<Guest> guestMapper = (rs, rowNum) ->
            new Guest(rs.getLong("id"), rs.getString("name"), rs.getString("address"), rs.getDate("date_of_birth"));
}
