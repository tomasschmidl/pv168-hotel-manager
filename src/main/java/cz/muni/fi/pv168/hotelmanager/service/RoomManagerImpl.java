package cz.muni.fi.pv168.hotelmanager.service;

import cz.muni.fi.pv168.hotelmanager.entity.Room;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.List;

import javax.sql.DataSource;

public class RoomManagerImpl implements RoomManager {

    private JdbcTemplate jdbc;

    public RoomManagerImpl(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    public void createRoom(Room room) {
        SimpleJdbcInsert insertRoom = new SimpleJdbcInsert(jdbc)
                .withTableName("rooms").usingGeneratedKeyColumns("id");

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("bed_count", room.getBedCount())
                .addValue("room_type", room.getRoomType().toString())
                .addValue("clean", room.isClean())
                .addValue("price_per_night", room.getPricePerNight());

//        BeanPropertySqlParameterSource bpsps = new BeanPropertySqlParameterSource(Room.class);
//        bpsps.registerSqlType("status", Types.VARCHAR);
//        parameters = bpsps;

        Number id = insertRoom.executeAndReturnKey(parameters);
        room.setId(id.longValue());
    }

    public Room getRoomById(Long id) {
        return jdbc.queryForObject("SELECT * FROM rooms WHERE id=?", roomMapper, id);
    }

    public List<Room> getAllRooms() {
        return jdbc.query("SELECT * FROM rooms", roomMapper);
    }

    public void updateRoom(Room room) {
        jdbc.update("UPDATE rooms SET bed_count=?,room_type=?,clean=?,price_per_night=?",
                room.getBedCount(), room.getRoomType().toString(), room.isClean(), room.getPricePerNight());
    }

    public void deleteRoom(Long id) {
        jdbc.update("DELETE FROM rooms WHERE id=?", id);
    }

    private RowMapper<Room> roomMapper = (rs, rowNum) ->
            new Room(rs.getLong("id"),
                    rs.getInt("bed_count"),
                    Room.Type.valueOf(rs.getString("room_type")),
                    rs.getBoolean("clean"),
                    rs.getLong("price_per_night"));
}
