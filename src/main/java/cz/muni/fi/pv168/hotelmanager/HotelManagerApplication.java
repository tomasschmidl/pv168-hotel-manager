package cz.muni.fi.pv168.hotelmanager;

import cz.muni.fi.pv168.hotelmanager.service.GuestManager;
import cz.muni.fi.pv168.hotelmanager.service.GuestManagerImpl;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.IOException;

import javax.sql.DataSource;

public class HotelManagerApplication {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        GuestManager guestManager = context.getBean(GuestManager.class);

        guestManager.getAllGuests().forEach(System.out::println);
    }

    @Configuration
    @EnableTransactionManagement
    @PropertySource("classpath:application.properties")
    public static class SpringConfig {

        @Autowired
        Environment env;

        @Bean
        public DataSource dataSource() {
            BasicDataSource bds = new BasicDataSource();
            bds.setDriverClassName(env.getProperty("jdbc.driver"));
            bds.setUrl(env.getProperty("jdbc.url"));
            bds.setUsername(env.getProperty("jdbc.user"));
            bds.setPassword(env.getProperty("jdbc.password"));
            return bds;
        }

        @Bean
        public PlatformTransactionManager transactionManager() {
            return new DataSourceTransactionManager(dataSource());
        }

        @Bean
        public GuestManager guestManager() {
            return new GuestManagerImpl(dataSource());
        }

//        @Bean
//        public RoomManager roomManager() {
//            return new RoomManagerImpl(new TransactionAwareDataSourceProxy(dataSource()));
//        }
//
//        @Bean
//        public ReservationManager reservationManager() {
//            ReservationManagerImpl reservationManager = new ReservationManagerImpl(dataSource());
//            reservationManager.setRookManager(roomManager());
//            reservationManager.setGuestManager(guestManager());
//            return leaseManager;
//        }
    }
}
