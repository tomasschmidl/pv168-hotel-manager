CREATE TABLE guests (
   id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
   name VARCHAR(20),
   address VARCHAR(50),
   date_of_birth DATE
);

CREATE TABLE rooms (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    bed_count INT,
    room_type VARCHAR(10),
    clean BOOLEAN,
    price_per_night INT
);

CREATE TABLE reservations (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    start_date DATE,
    end_date DATE,
    room_id INT,
    guest_id INT,
    is_over BOOLEAN
);